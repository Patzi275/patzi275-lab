# Challs

**Veuillez toujours mettre à jour ce fichier lorsque vous ajoutez un fichier dans ce dossier, pour s'assurer que sa disparition n'est pas du à une suppression accidentel par le push de quelqu'un.****

**Si un fichier a été supprimé intentionnellement, veuillez ne pas supprimer sa ligne dans ce fichier mais simplement le marquer comme supprimé et ajouter la raison de la suppression.****

#### Modèle d'écriture dans ce fichier

* Nom_Du_Challenge (**Categorie**) (**Si_Supprimé**)
	* url -- date -- Niveau_Du_Challenge
	* Solutions :
		* Solution(s) externes: Nombre_de_solution  
Numero_De_La_Solution. url -- date -- Accessibilité
		* Propre solutions(s) : Nombre_de_solution  
Numero_De_La_Solution. RépertoireChalls/Nom_Du_Fichier
	* (Raison(s)_Si_Supprimé)
	
	
PS: L'accessibilité est la note traduisant la facilité de compréhension d'une solution . Elle varie de 1 à 20

**Ex :**

* sudo - faiblesse de configuration (**App-Script**) (**Supprimé**)
	* ( https://www.defis.org/App-Script/sudo-faiblesse-de-configuration -- 3/01/2022 14:43:28 ) -- Très facile
	* Solutions :
		* Solution(s) externes::1  
			1.( https://www.defis.org/App-Script/Solutions/sudo-faiblesse-de-configuration -- L3/01/2022 14:43:28 ) -- 12
		* Propre solutions(s) : 0

<hr>

# Docs

**Veuillez toujours mettre à jour ce fichier lorsque vous ajoutez un fichier dans ce dossier, pour s'assurer que sa disparition n'est pas du à une suppression accidentel par le push de quelqu'un.****

**Si un fichier a été supprimé intentionnellement, veuillez ne pas supprimer sa ligne dans ce fichier mais simplement le marquer comme supprimé et ajouter la raison de la suppression****

#### Modèle d'écriture dans ce fichier
* Répertoire_Du_Doc/Nom_Du_Doc (**Langue**) (**Si_Supprimé**)
	* url -- date	
	* Categorie(s) :**catégorie**
	* ( Raison(s)_Si_Supprimé )




**Ex :**

* Hacking, sécurité et tests d'intrusion avec Metasploit (**FR**) (**DELETED**)
	* https://www.pearson.ch/download/media/9782744066931_SP_1_2.pdf -- 15:05:50
	* Categorie : Payload
	* Document possédant beaucoup de mensonges
	
<hr>

# Tools

**Veuillez toujours mettre à jour ce fichier lorsque vous ajoutez un fichier dans ce dossier, pour s'assurer que sa disparition n'est pas du à une suppremssion accidentel par le push de quelqu'un.****

**Si un fichier a été supprimé intentionnellement, veuillez ne pas supprimer sa ligne dans ce fichier mais simplement le marquer comme supprimé et ajouter la raison de la suppression****

#### Modèle d'écriture dans ce fichier

* Répertoire/Fichier (**Si_Supprimé**)
	* url -- date
	* Description explicite et utilisation :
	    > Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint exercitationem expedita error nobis minus maiores molestiae saepe laboriosam non modi nihil, voluptatibus dicta ullam, aperiam, veniam possimus dolores! Eaque, veritatis!
	* ( Raison(s)_Si_Supprimé )


**Ex :**

* Scripts/Bash/Reverse_tool_1.sh (**Supprimé**)

	* https://le-hacker.fr/reverse_tool.sh -- 02/01/2022 14:45:25
 	* Description explicite et utilisation :
	    > Ce script permet de prendre le contrôle du machine distante par ssh en exploitant une vulnérabilité du protocole http dans sa version 2.4.
Pour l'utiliser , ........
	* Il a été remplacé par Scripts/Bash/Reverse_tool_2.sh qui exploite plus vulnérabilité et est plus à jour
